package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.entity.Usuario;

@Repository
public interface UsuarioDao extends JpaRepository<Usuario, String>{

}
