package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.entity.Compromisso;

@Repository
public interface CompromissoDao extends JpaRepository<Compromisso, Integer>{

}
