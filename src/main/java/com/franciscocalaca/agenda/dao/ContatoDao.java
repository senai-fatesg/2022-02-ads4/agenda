package com.franciscocalaca.agenda.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.franciscocalaca.agenda.entity.Contato;

@Repository
public interface ContatoDao extends JpaRepository<Contato, Integer>{

}
