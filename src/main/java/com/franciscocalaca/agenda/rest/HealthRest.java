package com.franciscocalaca.agenda.rest;

import java.security.Principal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/health")
public class HealthRest {

	@GetMapping
	public Map<String, Object> get(Principal principal){
		Map<String, Object> result = new HashMap<>();
		result.put("status", "OK");
		result.put("moment", new Date());
		result.put("user", principal.getName());
		return result;
	}
	
}
