package com.franciscocalaca.agenda.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.franciscocalaca.agenda.dao.CompromissoDao;
import com.franciscocalaca.agenda.entity.Compromisso;


@RestController
@RequestMapping("/compromisso")
public class CompromissoRest {

	@Autowired
	private CompromissoDao compromissoDao;
	
	@GetMapping
	public List<Compromisso> get(){
		return compromissoDao.findAll();
	}
	
	@GetMapping("/{id}")
	public Compromisso get(@PathVariable("id") Integer id){
		return compromissoDao.findById(id).get();
	}
	
	@PostMapping
	public void post(@RequestBody Compromisso contato) {
		compromissoDao.save(contato);
	}
	
	@DeleteMapping("/{id}")
	public void delete(@PathVariable("id") Integer id) {
		compromissoDao.deleteById(id);
	}
}
