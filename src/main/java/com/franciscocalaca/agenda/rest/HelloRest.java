package com.franciscocalaca.agenda.rest;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.management.RuntimeErrorException;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/hello")
public class HelloRest {

	@ApiResponses(value = {
		    @ApiResponse(code = 200, message = "Retorna o texto OK e a data/hora"),
		    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
		    @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
		})
	@GetMapping
	@ApiOperation(value = "Exemplo criado na primeira aula de WS Rest")
	public String get(Principal principal) {
//		throw new RuntimeException("Erro geral gerado como exemplo");
		return "OK " + new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZZ").format(new Date()) + " " + principal.getName();
	}
	
}
