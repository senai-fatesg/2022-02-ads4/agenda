package com.franciscocalaca.agenda.aulaautowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class AutoWiredClasseComponent2 {

	@Autowired
	public AutoWiredClasseComponent2(ClasseComponent cmp) {
		System.out.println("classe AutoWiredClasseComponent instanciado...");
		System.out.println(cmp.getAttr());
		cmp.setAttr("Alterado 2");
	}
	
}
