package com.franciscocalaca.agenda.aulaautowired;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class ClasseComponent {
	
	private String attr;
	
	public ClasseComponent() {
		System.out.println("Construtor ClasseComponent executado");
	}

	public String getAttr() {
		return attr;
	}

	public void setAttr(String attr) {
		this.attr = attr;
	}
	
	
}
