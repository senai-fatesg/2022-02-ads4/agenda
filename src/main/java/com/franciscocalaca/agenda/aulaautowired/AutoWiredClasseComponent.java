package com.franciscocalaca.agenda.aulaautowired;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AutoWiredClasseComponent {

	@Autowired
	public AutoWiredClasseComponent(ClasseComponent cmp) {
		System.out.println("classe AutoWiredClasseComponent instanciado...");
		System.out.println(cmp.getAttr());
		cmp.setAttr("Alterado 1");
	}
	
}
